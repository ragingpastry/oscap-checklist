# Ansible Collection - dingus9.oscap_checklist

Documentation for the collection.


## run deps:

### OS deps:

- oscap-scanner
- python
- ansible 9.x - 10.x

### Python

#### to do xccdf diff

- dictdiffer


## Test deps

## Python

- mock
- pytest
- pytest-cov
- pytest-mock
- pytest-xdist
- pyyaml
