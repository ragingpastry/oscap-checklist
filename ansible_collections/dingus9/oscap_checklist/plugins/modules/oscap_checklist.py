#!/usr/bin/python

# Copyright: (c) 2020, Nick Shobe <nickshobe@gmail.com>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

import os

import xml.etree.ElementTree as ET

from ansible.module_utils.basic import AnsibleModule

from dictdiffer import diff

DOCUMENTATION = r'''
---
module: oscap_checklist

short_description: Represents an oscap client checklist xccdf scan checklist and results

version_added: "0.0.1"

description: work with a oscap checklist and results to run or evaluate a xccdf eval

options:
    checklist:
        description: Specify a path to a SCAP (xccdf/1.2) benchmark checklist xml file
                     on the target host.
            - mutually_exclusive with from_results
            - requires oscap on the path or oscap_bin
        required: false
        type: path
    checklist_profile:
        description: Specify the profile for a SCAP benchmark checklist
            - required by checklist
            - same value as `oscap xccdf eval --profile $checklist_profile`
        type: str

    from_results:
        description: Pass in a checklist SCAP scan results (xccdf/1.2) xml as a
                     string or path to xml on target
            - mutually_exclusive with checklist
            - must be well formed http://checklists.nist.gov/xccdf/1.2
        type: str|path
        required: false
    compare_results:
        description: Pass in a checklist SCAP scan results (xccdf/1.2) xml as a string
            - used to compare to the current scan results or from_results
            - must be well formed http://checklists.nist.gov/xccdf/1.2
            - switched the module to compare mode
            - mutually_exclusive with rule
        type: str
        required: false
    results_file:
        description: Save the results (xccdf/1.2) xml to a file on the target
        type: path
        required: false
    state:
        description: Ensure rule is a specified state
            - one of ['fail', 'pass', 'skipped', 'notapplicable']
            - mutually_exclusive with compare_results
        type: str
        required: false
    rule:
        description: Specify a rule ID or a list of rule IDs to check state on
            - requres state
        type: list
        required: false
    oscap_bin:
        description: Specify the oscap bin path
            - only used with checklist
        type: path
        required: false
    oscap_eval_args:
        description: Specify oscap binary command operations
            -  default ['xccdf', 'eval']
        type: list
        required: false

author:
    - Nick Shobe <nickshobe@gmail.com>
'''

EXAMPLES = r'''
- name: drop xml benchmark
  file:
    src: files/U_RHEL_7_V3R1_STIG_SCAP_1-2_Benchmark.xml
    dst: test-oscap_checklist/U_RHEL_7_V3R1_STIG_SCAP_1-2_Benchmark.xml
    state: present
- name: check system stig rules pass
  oscap_checklist:
    checklist: test-oscap_checklist/U_RHEL_7_V3R1_STIG_SCAP_1-2_Benchmark.xml
    checklist_profile: xccdf_mil.disa.stig_profile_MAC-3_Sensitive
    rule:
        - xccdf_mil.disa.stig_rule_SV-204631r505924_rule
        - xccdf_mil.disa.stig_rule_SV-204632r505924_rule
    state: pass
  register: first_checklist
- name: do the things
  file:
    ...
- name: check system stig didnt change
  oscap_checklist:
    checklist: test-oscap_checklist/U_RHEL_7_V3R1_STIG_SCAP_1-2_Benchmark.xml
    checklist_profile: xccdf_mil.disa.stig_profile_MAC-3_Sensitive
    compare_results: {{ first_checklist.checklist_results }}
  register: this_result
  failed_when: thist_result.diffs | length(0)
'''

RETURN = r'''
messages:
    description: Text messages about the internal state and mode of this module
    type: str
    returned: always
    sample: this is a string that is all
unmatched_rules:
    description: A list of rules that do not match the value of "state"
    type: list
    returned: always
    sample: ["xccdf_mil.disa.stig_rule_SV-214799r505924_rule",]
state:
    description: The state if any to ensure all specified rules match
    type: str
    returned: always
    sample: pass|fail|notapplicable
matched_rules:
    description: The list of rules that were matched in this run
    type: list
    returned: always
    sample: ["xccdf_mil.disa.stig_rule_SV-214799r505924_rule"]
diffs:
    description: A list of dictdiffer results indicating any changes between
                 from_results and compare_results xml after being parsed to dicts
    type: list
    returned: aways
    sample: |
        [('change', ['xccdf_mil.disa.stig_rule_SV-204633r505924_rule'],
         ('fail', 'fail-diffstate'))]
checklist_results:
    description: Checklist xml either from a oscap run or passed in on from_results
    type: str
    returned: always
    sample: "<XML>"
compare_results:
    description: Checklist xml either from a oscap run or passed in on compare_results
    type: str
    returned: always
    sample: "<XML>"
'''


def parse_xccdf_results(result_xml):
    ns_map = {
        'xccdf': 'http://checklists.nist.gov/xccdf/1.2'
    }
    root_el = ET.fromstring(result_xml)

    # test_results = root_el.find('./xccdf:TestResult', ns_map)
    groups = root_el.findall('./xccdf:Group', ns_map)

    groups_dict = {}
    for group in groups:
        group_id = group.attrib['id']
        groups_dict[group_id] = {
            'id': group_id,
            'title': group.find('./xccdf:title', ns_map).text,
            'description': group.find('./xccdf:title', ns_map).text,
            'rules': {},
            'results': []
        }

        groups_dict[group_id]['results'] = []

        for rule in group.findall('./xccdf:Rule', ns_map):
            rule_dict = {}
            rule_id = rule.attrib['id']
            for attrib in ('selected', 'weight', 'severity'):
                rule_dict[attrib] = rule.attrib[attrib]

            for child_tag in ('version', 'title', 'description', 'fixtext'):
                rule_dict[child_tag] = rule.find(
                    './xccdf:{child_tag}'.format(child_tag=child_tag),
                    ns_map
                ).text

            rule_dict['fix'] = rule.find('./xccdf:fix', ns_map).attrib['id']

            dc_ns = {
                'dc': 'http://purl.org/dc/elements/1.1/'
            }

            t_info_dict = {}
            for tinfo in rule.findall('./xccdf:reference', ns_map):
                t_info_dict['title'] = tinfo.find('./dc:title', dc_ns).text
                t_info_dict['publisher'] = tinfo.find('./dc:publisher', dc_ns).text
                t_info_dict['type'] = tinfo.find('./dc:type', dc_ns).text

            rule_dict['target_info'] = t_info_dict
            groups_dict[group_id]['rules'][rule_id] = rule_dict

            for result in root_el.findall(
                    './xccdf:TestResult/xccdf:rule-result[@idref=\'{rule_id}\']'.format(
                        rule_id=rule_id
                    ),
                    ns_map
                        ):
                rule_dict['result'] = result.find('./xccdf:result', ns_map).text
                groups_dict[group_id]['results'].append({
                    'idref': result.attrib['idref'],
                    'result': result.find('./xccdf:result', ns_map).text
                })
    return groups_dict


def rule_to_state(results_dict):

    rule_states = {}

    for g_id, group_data in results_dict.items():
        for rule_id, rule in group_data['rules'].items():
            rule_states[rule_id] = rule['result']

    return rule_states


def eval_rules(results_dict, rules, state):

    rule_states = rule_to_state(results_dict)

    rule_failures = []
    rule_matches = []
    for rule in rules:
        if rule_states[rule] != state:
            rule_failures.append(rule)
        else:
            rule_matches.append(rule)

    return rule_failures, rule_matches


def compare_results(results, prev_results):

    res_rules = rule_to_state(results)
    prev_rules = rule_to_state(prev_results)

    return list(diff(res_rules, prev_rules))


def run_module():  # noqa: C901

    module = AnsibleModule(
        dict(checklist=dict(type='path', required=False),
             checklist_profile=dict(type='str', required=False),
             from_results=dict(type='str', required=False),
             compare_results=dict(type='str', required=False),
             results_file=dict(type='path', required=False),
             state=dict(choices=['fail', 'pass', 'skipped', 'notapplicable'],
                        type='str',
                        required=False),
             rule=dict(type='list', required=False),
             oscap_bin=dict(type='path', required=False),
             oscap_eval_args=dict(type='list', required=False,
                                  default=['xccdf', 'eval'])),
        required_one_of=[['checklist', 'from_results']],
        mutually_exclusive=[['checklist', 'from_results'],
                            ['state', 'compare_results']],
        required_together=[['checklist', 'checklist_profile']],
        required_by={'state': 'rule', 'oscap_bin': 'checklist'},
        supports_check_mode=True
    )
    messages = []
    changed = False

    if module.check_mode:
        if module.params['results_file']:
            messages.append('check_mode not writing to results_file')
            module.params['results_file'] = ''

    if module.params['checklist']:
        # execute a checklist scan and save results for processing
        if module.params['oscap_bin']:
            oscap_args = [module.params['oscap_bin']]
        else:
            oscap_args = [module.get_bin_path('oscap', False)]

        oscap_args.extend(module.params['oscap_eval_args'])
        if module.params['results_file']:
            out_results = module.params['results_file']
            changed = True
        else:
            out_results = '-'

        oscap_args.extend(['--results', out_results])
        oscap_args.extend(['--profile', module.params['checklist_profile']])
        oscap_args.append(module.params['checklist'])

        rc, stdout, stderr = module.run_command(oscap_args, check_rc=False)

        if module.params['results_file']:
            with open(module.params['results_file']) as outfile:
                from_results = outfile.read()
        else:
            from_results = stdout
            # Strip off the top messages
            from_results = from_results[from_results.find('<?xml'):]
            messages.append('no xml output on oscap stdout')
            if not len(from_results.strip()):
                module.fail_json(
                    ', '.join(messages),
                    changed=changed,
                    diffs=None,
                    matched_rules=None,
                    messages=messages,
                    unmatched_rules=None,
                    rules_parsed=None,
                    state=module.params['state'])

    else:
        if os.path.isfile(module.params['from_results']):
            with open(module.params['from_results']) as results:
                from_results = results.read()
        # elif module.params['from_results'][:5] == '<?xml':
        #     from_results = module.params['from_results']
        else:
            messages.append('from_results does not exist or '
                            'is not valid xml')
            module.fail_json(
                ', '.join(messages),
                changed=changed,
                diffs=None,
                matched_rules=None,
                messages=messages,
                unmatched_rules=None,
                rules_parsed=None,
                state=module.params['state'])

    results_dict = parse_xccdf_results(from_results)

    if module.params['compare_results']:
        messages.append('compare mode')

        if os.path.exists(module.params['compare_results']):
            with open(module.params['compare_results']) as results:
                prev_results = results.read()
        else:  # assume contents is an xccdf xml
            prev_results = module.params['compare_results']

        prev_results_dict = parse_xccdf_results(prev_results)
        diffs = compare_results(results_dict, prev_results_dict)

        if len(diffs):
            changed = True

        module.exit_json(
            changed=changed,
            diff=diffs,
            matched_rules=None,
            messages=messages,
            unmatched_rules=None,
            rules_parsed=None,
            state=None)
    else:
        messages.append('rule state mode')
        rule_failures, rule_matches = eval_rules(results_dict,
                                                 module.params['rule'],
                                                 module.params['state'])
        if len(rule_failures):
            module.fail_json(
                ', '.join(messages),
                changed=changed,
                diffs=None,
                matched_rules=rule_matches,
                messages=messages,
                unmatched_rules=rule_failures,
                state=module.params['state'])

        else:
            module.exit_json(
                changed=changed,
                diffs=None,
                matched_rules=rule_matches,
                messages=messages,
                unmatched_rules=rule_failures,
                state=module.params['state'])


if __name__ == '__main__':  # pragma: no cover
    run_module()
